package com.example.chapter4ii.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.chapter4ii.model.Note

@Dao interface NoteDao {

    @Query("SELECT * FROM user_note ORDER BY id ASC")
    fun readAllAdata(): LiveData<List<Note>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addNote(note: Note)

    @Update
    suspend fun updateNote(note: Note)

    @Delete
    suspend fun deleteNote(note: Note)
}