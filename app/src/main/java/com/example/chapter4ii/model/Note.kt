package com.example.chapter4ii.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_note")
data class Note (
    @PrimaryKey(autoGenerate = true) val id: Int,
    val judul: String,
    val note: String
    ): Parcelable