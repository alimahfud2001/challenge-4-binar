package com.example.chapter4ii.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.chapter4ii.R
import com.example.chapter4ii.fragments.HomepageFragmentDirections
import com.example.chapter4ii.model.Note
import com.example.chapter4ii.viewmodel.NoteViewModel
import kotlinx.android.synthetic.main.list_item.view.*

class NoteAdapter: RecyclerView.Adapter<NoteAdapter.ViewHolder>() {
    private var noteList = emptyList<Note>()
    class ViewHolder(view: View): RecyclerView.ViewHolder(view){}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = noteList[position]
        holder.itemView.title.text = currentItem.judul
        holder.itemView.note.text = currentItem.note

        holder.itemView.btnedit.setOnClickListener {
            val action = HomepageFragmentDirections.actionHomepageFragment2ToUpdateFragment(currentItem)
            it.findNavController().navigate(action)
        }

        holder.itemView.btndelete.setOnClickListener {
            val action = HomepageFragmentDirections.actionHomepageFragment2ToDeleteFragment(currentItem)
            it.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    fun setData(note: List<Note>){
        this.noteList = note
        notifyDataSetChanged()
    }
}