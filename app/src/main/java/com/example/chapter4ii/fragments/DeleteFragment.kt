package com.example.chapter4ii.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.chapter4ii.databinding.FragmentDeleteBinding
import com.example.chapter4ii.viewmodel.NoteViewModel

class DeleteFragment : DialogFragment() {
    private var _binding: FragmentDeleteBinding? = null
    private val binding get() = _binding!!
    private lateinit var mNoteViewModel: NoteViewModel
    private val args by navArgs<DeleteFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)
        binding.btndeleteyes.setOnClickListener {
            deleteData()
            dialog?.dismiss()
        }
        binding.btndeleteno.setOnClickListener {
            dialog?.dismiss()
        }
    }

    private fun deleteData() {
        mNoteViewModel.deleteNote(args.currentNote)
        Toast.makeText(requireContext(), "Catatan ${args.currentNote.judul} Berhasil Dihapus", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}