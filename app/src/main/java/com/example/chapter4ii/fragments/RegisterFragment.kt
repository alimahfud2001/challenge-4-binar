package com.example.chapter4ii.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.chapter4ii.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "sharedpreferencesfile"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

        binding.btnDaftar.setOnClickListener {
            val username = binding.ti1.editText?.text.toString()
            val email = binding.ti2.editText?.text.toString()
            val password = binding.ti3.editText?.text.toString()
            val confirmPassword = binding.ti4.editText?.text.toString()

            if (username.isNullOrEmpty()||email.isNullOrEmpty()||password.isNullOrEmpty()||confirmPassword.isNullOrEmpty())
                Toast.makeText(context, "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                val set  = sharedPreferences.getStringSet(email, null)
                if (set?.elementAt(1) != null) {
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setTitle("Pendaftaran Gagal")
                    dialog.setMessage("Email sudah terdaftar")
                    dialog.setPositiveButton("Ok"){ dialogInterface, p1 -> }
                    dialog.show()
                }
                else {
                    if (password != confirmPassword) {
                        Toast.makeText(context, "Password tidak cocok", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        val editor: SharedPreferences.Editor = sharedPreferences.edit()
                        val set = setOf(username, email, password)
                        editor.putStringSet(email, set)
                        editor.apply()
                        val dialog = AlertDialog.Builder(requireContext())
                        dialog.setTitle("Pendaftaran Berhasil")
                        dialog.setMessage("Silahkan Login")
                        dialog.setPositiveButton("Login"){ dialogInterface, p1 ->
                            it.findNavController().popBackStack()
                        }
                        dialog.show()
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}