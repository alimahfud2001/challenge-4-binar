package com.example.chapter4ii.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.chapter4ii.databinding.FragmentUpdateBinding
import com.example.chapter4ii.model.Note
import com.example.chapter4ii.viewmodel.NoteViewModel

class UpdateFragment : DialogFragment() {
    private var _binding: FragmentUpdateBinding? = null
    private val binding get() = _binding!!
    private lateinit var mNoteViewModel: NoteViewModel
    private val args by navArgs<DeleteFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUpdateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.et1.setText(args.currentNote.judul)
        binding.et2.setText(args.currentNote.note)

        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)
        binding.btnupdate.setOnClickListener {
            updateData(args.currentNote.id)
            dialog?.dismiss()
        }
    }

    private fun updateData(id: Int){
        val title = binding.et1.text.toString()
        val note = binding.et2.text.toString()

        if (inputCheck(title, note)){
            val updatedNote = Note(id, title, note)
            mNoteViewModel.updateNote(updatedNote)
            Toast.makeText(view?.context, "Catatan Berhasil Diupdate", Toast.LENGTH_SHORT).show()
        }
        else
            Toast.makeText(view?.context, "Isi Detail", Toast.LENGTH_SHORT).show()
    }
    private fun inputCheck(title: String, note: String): Boolean{
        return !(title.isNullOrEmpty()||note.isNullOrEmpty())
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}