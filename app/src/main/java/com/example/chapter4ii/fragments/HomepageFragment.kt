package com.example.chapter4ii.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chapter4ii.R
import com.example.chapter4ii.adapter.NoteAdapter
import com.example.chapter4ii.viewmodel.NoteViewModel
import com.example.chapter4ii.databinding.FragmentHomepageBinding
import com.example.chapter4ii.model.Note
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_delete.view.*
import kotlinx.android.synthetic.main.fragment_homepage.view.*

class HomepageFragment : Fragment() {
    private var _binding: FragmentHomepageBinding? = null
    private val binding get() = _binding!!
    private lateinit var mNoteViewModel: NoteViewModel
    private val sharedPrefFile = "sharedpreferencesfile"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomepageBinding.inflate(inflater, container, false)
        return binding.root}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val email = sharedPreferences.getString("isLoggedIn", null)
        val set = sharedPreferences.getStringSet(email, null)
        val adapter = NoteAdapter()
        val recyclerview = view.recyclerview
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(requireContext())

        binding.tv1.setText("Welcome, ${set?.elementAt(1)}!")
        binding.tvlogout.setOnClickListener {
            val view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_delete, null, false)
            val dialogBuilder = AlertDialog.Builder(requireContext())
            dialogBuilder.setView(view)
            val dialog = dialogBuilder.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            view.btndeleteyes.setText("Logout")
            view.tvdelete.setText("Logout Akun?")
            val _it = it
            view.btndeleteyes.setOnClickListener {
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putStringSet("isLoggedIn", null)
                editor.apply()
                _it.findNavController().navigate(R.id.action_homepageFragment2_to_loginFragment)
                dialog?.dismiss()
            }
            view.btndeleteno.setOnClickListener {
                dialog?.dismiss()
            }
        }

        mNoteViewModel= ViewModelProvider(this).get(NoteViewModel::class.java)
        mNoteViewModel.readAllData.observe(viewLifecycleOwner, Observer { note ->
            adapter.setData(note)
        })

        binding.floatingActionButton.setOnClickListener {
            val view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_add, null, false)
            val dialogBuilder = AlertDialog.Builder(requireContext())
            dialogBuilder.setView(view)
            val dialog = dialogBuilder.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            view.btnadd.setOnClickListener {
                val title = view.et1.text.toString()
                val note = view.et2.text.toString()
                insertDataToDatabase(title, note, dialog)
            }
        }
    }
    private fun insertDataToDatabase(title: String, note: String, dialog: AlertDialog) {
        if (inputCheck(title, note)){
            val note = Note(0, title, note)
            mNoteViewModel.addNote(note)
            Toast.makeText(requireContext(), "Catatan Berhasil Ditambahkan", Toast.LENGTH_SHORT).show()
            dialog.dismiss()
        }
        else Toast.makeText(requireContext(), "Isi Semua", Toast.LENGTH_SHORT).show()
    }

    private fun inputCheck(title: String, note: String): Boolean{
        return !(title.isNullOrEmpty()||note.isNullOrEmpty())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}