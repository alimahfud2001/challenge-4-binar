package com.example.chapter4ii.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.chapter4ii.model.Note
import com.example.chapter4ii.viewmodel.NoteViewModel
import com.example.chapter4ii.databinding.FragmentAddBinding

class AddFragment : DialogFragment() {
    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!
//    private lateinit var mNoteViewModel: NoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)
//
//        binding.btnadd.setOnClickListener {
//            insertDataToDatabase()
//        }
    }

//    private fun insertDataToDatabase() {
//        val title = binding.et1.text.toString()
//        val note = binding.et2.text.toString()
//
//        if (inputCheck(title, note)){
//            val note = Note(0, title, note)
//            mNoteViewModel.addNote(note)
//            Toast.makeText(requireContext(), "Catatan Berhasil Ditambahkan", Toast.LENGTH_SHORT).show()
//        }
//        else Toast.makeText(requireContext(), "Isi Semua", Toast.LENGTH_SHORT).show()
//
//    }
//
//    private fun inputCheck(title: String, note: String): Boolean{
//        return !(title.isNullOrEmpty()||note.isNullOrEmpty())
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}