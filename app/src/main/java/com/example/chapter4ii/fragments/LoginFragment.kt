package com.example.chapter4ii.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.chapter4ii.R
import com.example.chapter4ii.databinding.FragmentLoginBinding
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "sharedpreferencesfile"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root}

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

        binding.btnLogin.setOnClickListener {
            val email = binding.ti1.editText?.text.toString()
            val password = binding.ti2.editText?.text.toString()
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            val set  = sharedPreferences.getStringSet(email, null)
            if (email.isEmpty()||password.isEmpty())
                Toast.makeText(requireContext(), "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                if (set?.elementAt(2) == null)
                    Toast.makeText(requireContext(), "Email Tidak Terdaftar", Toast.LENGTH_SHORT).show()
                else{
                    if (password != set.elementAt(0))
                        Toast.makeText(requireContext(), "Password Salah", Toast.LENGTH_SHORT).show()
                    else {
                        editor.putString("isLoggedIn", email)
                        editor.apply()
                        it.findNavController().navigate(R.id.action_loginFragment_to_homepageFragment2)
                    }
                }
            }
        }

        binding.tvblmpnya.setOnClickListener{
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment2)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}