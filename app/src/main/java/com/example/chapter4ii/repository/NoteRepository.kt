package com.example.chapter4ii.repository

import androidx.lifecycle.LiveData
import com.example.chapter4ii.model.Note
import com.example.chapter4ii.data.NoteDao

class NoteRepository(private val noteDao: NoteDao) {
    val readAllData : LiveData<List<Note>> = noteDao.readAllAdata()

    suspend fun addNote(note: Note){
        noteDao.addNote(note)
    }

    suspend fun updateNote(note: Note){
        noteDao.updateNote(note)
    }

    suspend fun deleteNote(note: Note){
        noteDao.deleteNote(note)
    }
}